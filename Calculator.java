public class Calculator {
	
	public static void main(String[] args) {
		
		Calculate(20, 10, 'd');
		
	}
	
	public static double Calculate (double a, double b, char action) {
		if (action == 'a') {
			return a+b;
		}
		else if (action == 's') {
			return a-b;
		}
		else if (action == 'm') {
			return a*b;
		}
		else if (action == 'd') {
			return a/b;
		}
		else return 0.0;
	}
	
}
